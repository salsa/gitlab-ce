# frozen_string_literal: true

Rack::Attack.blocklist('block from redis') do |req|
  ::Gitlab::Redis::SharedState.with do |redis|
    begin
      ip = IPAddr.new(req.ip)

      keys = [req.ip]
      if ip.ipv4?
        (24..16).step(-1).each do |mask|
          ipmasked = ip.mask(mask)
          keys << "#{ipmasked}/#{ipmasked.prefix}"
        end
      elsif ip.ipv6?
        (48..32).step(-1).each do |mask|
          ipmasked = ip.mask(mask)
          keys << "#{ipmasked}/#{ipmasked.prefix}"
        end
      end

      reason = redis.hmget('blacklist', *keys).find { |r| !r.nil? }
      if reason
        if reason != ''
          req.env['rack_attack.salsa.blocklist.reason'] = reason
        end

        true
      end

    rescue IPAddr::AddressFamilyError
    end
  end
end

Rack::Attack.blocklisted_response = lambda do |env|
  reason = env['rack_attack.salsa.blocklist.reason']
  if reason
    text = "Forbidden, #{reason}\n"
  else
    text = "Forbidden\n"
  end
  [403, {'Content-Type' => 'text/plain'}, [text]]
end
